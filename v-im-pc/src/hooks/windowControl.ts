import { ipcRenderer, shell } from "electron";

const winControl = {
  openURL: (url: string): void => {
    shell.openExternal(url).catch((err) => {
      console.error(err);
    });
  },
  flashFrame: (): void => {
    ipcRenderer.send("flashFrame");
  },
  min: (): void => {
    ipcRenderer.send("min");
  },
  max: (): void => {
    ipcRenderer.send("max");
  },
  close: (): void => {
    ipcRenderer.send("close");
  },
  flashIcon: (): void => {
    ipcRenderer.send("flashIcon");
  },
  clearFlashIcon: (): void => {
    ipcRenderer.send("clearFlashIcon");
  },
  onSleep: (sleep: () => unknown): void => {
    ipcRenderer.once("SLEEP", sleep);
  },
  onResume: (resume: () => unknown): void => {
    ipcRenderer.once("RESUME", resume);
  },
};

export default winControl;
